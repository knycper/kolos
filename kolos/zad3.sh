#!/bin/bash
read wartosc

if [ "$wartosc" == "-a" ]; then
	date
elif [ "$wartosc" == "-b" ]; then
	ls -a
elif [ "$wartosc" == "-c" ]; then
	echo $USER
elif [ "$wartosc" == "-d" ]; then
	echo -n "Podaj nazwe katalogu ktory chcesz stworzyc: "
	read nazwa
	mkdir $nazwa
elif [ "$wartosc" == "-e" ]; then
	echo -n "Podaj nazwe pliku ktory chcesz usunac: "
	read nazwa
	rm -f $nazwa
elif [ "$wartosc" == "-f"  ]; then
	echo -n "Podaj nazwe pliku: "
	read nazwa
	head -n 10 $nazwa
elif [ "$wartosc" == "-g"  ]; then
	echo -n "Podaj nazwe pliku: "
	read nazwa1
	tail -n 10 $nazwa1
elif [ "$wartosc" == "-h" ]; then
	echo "Skrypt zostal napisany aby zdac kolosa z wstepu. do podania sa wartosci od \"-a do -j\". kazda wartosc wykonuje jakies zadanie. nie wiem czy o taka pomoc chodzilo. nizej uzywany jest inny help"
	help
elif [ "$wartosc" == "-i" ]; then
	find -type f | wc -l
elif [ "$wartosc" == "-j"  ]; then
	find -type f -name "*.txt"
else
	echo "Nie podano wartosci przypisanej do zadania"
fi

while true; do
	echo -n "Podaj zadanie ktore chcial bys wykonac(od 1 do 5). Aby skonczyc wcisnij 6: "
	read polecenie
	if [ "$polecenie" -eq 1 ]; then
		echo -n "Podaj liczbe"
		read liczba
		if ! [[ "$liczba" =~ ^[0-9]+$ ]]; then
			echo "Nie podano liczby"
		else
			suma=0
			for ((i=1; i<=$liczba; i++)); do
				suma=$(($suma+i))
			done
			echo $suma
		fi
	elif [ "$polecenie" -eq 2 ]; then
		echo -n "Podaj plik ktory chcesz odczytac: "
		read plik
		cat $plik | sort -r
	elif [ "$polecenie" -eq 3 ]; then
		echo -n "Podaj sciezke: "
		read sciezka
	elif [ "$polecenie" -eq 4 ]; then
		find | wc -l
	elif [ "$polecenie" -eq 5 ]; then
		echo -n "Podaj nazwe pliku: "
		read nazwa_pliku
		find -type f -name $nazwa_pliku
	elif [ "$polecenie" -eq 6 ]; then
		break
	else
		echo "Nie podano polecenia od 1 do 6"
	fi
done




